# nodejs - express - sequelize

## Finalidad
El proyecto debera recibir desde un ambiente externo nuevas Ventas ( producto, cantidad , valor , fecha ).
Debera poder ejecutarse diariamente un comando que genere una agrupación de las ventas por fecha y producto, teniendo como resultado la sumatoria final de valor y cantidad.
Finalmente, tendra un acceso a un panel donde se visualizará dicha información. Puede ser una tabla como un gráfico.
## Requerimentos 
* Crear un proyecto Nodejs con NPM 


## Modelos
Generar los modelos usando el ORM Sequelize con sus respectiva identidad en la Base de Datos (MYSQL).
Es de gran utilidad disponer de los respectivos comandos npm para la generación de tablas usando el syncronize de Sequelize.
Basta tener el Modelo de una Venta y el Sumarizado por Dia  para tener una solucion, pero no quita la utilización de otros modelos si asi lo crea conveniente.

## Webservices
Publicar un endpoint (REST) donde se puedan recibir nuevas Ventas.

## Comandos
El comando debera correrse con "npm" y recibira como parametro la fecha que desea procesar.
Realizara la agrupación necesaria para proveer a la vista de datos.

## Web
Con una vista alcanza para mostrar los resultados.
El frontend podrá ser un single page con React o Angular >=2.

## Entorno de pruebas
Debera existir un comando npm que levante el modulo express y publique tanto el webservice como el acceso al panel.


